const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController.js')

//Route for checking if the user's email already exists in the database
router.post('/checkEmail', (req, res) => {
    userController.checkEmailExists(req.body).then(result => res.send(result))
})

//Routes for user registration
router.post('/register', (req, res) => {
    userController.registerUser(req.body).then(result => res.send(result))
})

//Routes for authenticating a user
router.post('/login', (req, res) => {
    userController.loginUser(req.body).then(result => res.send(result))
})

//Routes for retrieving details of the user
router.post('/details', (req, res) => {
    userController.detailsUser(req.body).then(result => res.send(result))
})

module.exports = router;