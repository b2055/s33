//JSON web tokens are standard for sending info between our application in a secure manner
//will allows us to gain access to methods that will help us to create a JSON web token

const jwt = require('jsonwebtoken')
const secret = "CrushAkoNgCrushKo"

//JWT is a way of securely passing info from the server to the frontend or to the other parts of server
//info is kept secure through the use of the secret code
//only the system that knows the secret code that can decode the encrypted info.


//Token Creation
//Analogy: Pack the gift and provide a lock with the secret code as the key

module.exports.createAccessToken = (user) => {
    //Data will be received from the registration form
    //When the users log in, a token will be created with user's information

    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data, secret, {})
}