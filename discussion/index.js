//dependencies
const express = require('express')
const mongoose = require('mongoose')
//Allows us to control app's Cross Origin Resource Sharing Settings
const cors = require('cors')

//Routes
const userRoutes = require('./routes/userRoutes')

//server setup
const app = express()
const port = 4000

//Allows all resources/origin to access our backend application
app.use(cors())//Enable all CORS
app.use(express.json())
app.use(express.urlencoded({extended:true}))

//Defined the '/api/users' string to be included for all routes defined in the 'user' route file
app.use('/api/users', userRoutes)

//database connection
mongoose.connect("mongodb+srv://admin:Password30x__@cluster0.s5jd2.mongodb.net/batch144-booking_system?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})



let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database."))



app.listen(port, () => console.log(`Server is running at post ${port}`))