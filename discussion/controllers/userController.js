const User = require('../models/User.js')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')

/* 
    Check if the email already exists
    1. Use find() method to find duplicate emails
    2. Error handling
        -if no duplicate found, return false
        -else return true

IMPORTANT NOTE:

    best practice to return a result is to use a boolean or return an object/array of object. Because string is limited in our backend,
    and can't be connected to our frontend

*/

module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
        if(result.length > 0) {
            return true
        } else {
            // no duplicate email found
            return false
        }
    })
}

// User registration
/* 
Steps:
1. Create a new User object using the mongoose model and the info from the request body.
2. Make sure that the password is encrypted
3. Save the new user to the database
*/

module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        age: reqBody.age,
        gender: reqBody.gender,
        email: reqBody.email,
        //10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
        password: bcrypt.hashSync(reqBody.password, 10),
        mobileNo: reqBody.mobileNo
    })

    //save
    return newUser.save().then((user, error) => {
        //if registration failed
        if(error) {
            return false;
        } else {
            //user registration is successful
            return true;
        }
    })
}

//User authentication
/* 
Steps:
1. Check if the user email exists in our database. If user does not exist, return false
2. If the user exists, Compare the password provided in the login form with the password stored in the database
3. Generate/return a jsonwebtoken if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
    //findOne() method returns the first record in the collection that matches the search criteria
    //we use findOne() instead of 'find' method which returns all records that match the search criteria

    return User.findOne({ email:reqBody.email }).then(result => {
        //user does not exist
        if (result == null) {
            return false
        } else {
            //user exists
            //The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrived from the database and returns 'true'
            // or 'false' depending on the result
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            //If the password match/result of the above code is true
            if(isPasswordCorrect){
                //generate access token
                return { accessToken: auth.createAccessToken(result.toObject()) }
            } else {
                //password do not match
                return false
            }
        }
    })
}


//Retrieve user details
module.exports.detailsUser = (reqBody) => {

    console.log(reqBody.id)
    return User.findOne({ id:reqBody.id }).then((result, error) => {

        if (error) {
            console.log(error)
            return false
        }
        //user does not exist
        if (result == null) {
            return false
        } else {

            result.password = "";
            return result
        }
    })
}